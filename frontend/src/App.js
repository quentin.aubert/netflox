import './App.scss';
import Footer from './components/Footer';
import Nav from './components/Nav';
import Row from './components/Row';
import SignUp from './components/Signup';
import SignIn from './components/Signin';
import Pricing from './components/Pricing';

function App() {
  return (
    <div className="App">
      <Row title="Science-Fiction"/>
      {/* <Pricing /> */}
      <SignUp />
      <Footer/>
    </div>
  );
}

export default App;
