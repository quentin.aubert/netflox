import * as React from 'react';
import './Footer.scss'
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

function Copyright() {
  return (
    <Typography>
      {'Copyright © '}
      <Link color="inherit" href="#">
        Netflox
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Footer() {
  return (
    <div className="footer">
      <Copyright />
    </div>
  );
}
